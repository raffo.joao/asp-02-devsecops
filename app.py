from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return '''
        <form method="POST">
            <input type="text" placeholder="Número 1" name="num1" />
            <input type="text" placeholder="Número 2" name="num2" />
            <input type="submit" value="Somar" />
        </form>
    '''

@app.route('/', methods=['POST'])
def add():
    num1 = int(request.form['num1'])
    num2 = int(request.form['num2'])
    return str(num1 + num2)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
